const express = require('express');

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

let users = [
	{
		"username": "jenno",
		"password": "111"
	},
	{
		"username": "jenno2",
		"password": "222"
	}
	]

let message;

//home
app.get('/home', (request, response) => {
	message = 'Welcome to the home page'

	response.send(message);
});


//get users
app.get('/users', (request, response) => {

	response.send(users);
})


//delete users
app.delete('/delete-user', (request, response) => {
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			message = `User ${request.body.username} has been deleted`
			users.shift() 

		} else{
			message = 'User not found'
		}
	}
	response.send(message);
})




app.listen(port, () => console.log(`Server running at port: ${port}`));